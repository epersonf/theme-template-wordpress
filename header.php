<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <!-- Page information -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sample</title>

        <?php echo wp_head() ?>

        <!-- Reset always comes first -->
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/reset.css">

        <!-- CSS Injection -->
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">

        <!-- JavaScript Injection -->
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scriptfile.js"></script>
        
    </head>

    <body>

        <!-- Header -->
        <header>
            <nav>
                <?php
                    $args = array(
                        'menu' => 'principal',
                        'theme_location' => 'navigation',
                        'container' => false
                    );
                    echo wp_nav_menu();
                ?>
            </nav>
        </header>